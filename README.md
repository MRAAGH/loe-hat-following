# LoE hat following

Input:
![](https://mazie.rocks/files/follow-original.png)

Processed:
![](https://mazie.rocks/files/follow-reds.png)

Detected hats:
![](https://mazie.rocks/files/follow-thr.png)


![](https://mazie.rocks/files/follow-video-original.webm)
![](https://mazie.rocks/files/follow-video-reds.webm)
![](https://mazie.rocks/files/follow-video-thr.webm)

# FAQ

### How does this work?
Captures the screen and sends corresponding keypresses to your LoE game windows in real time.

### Is this allowed?
It is not against the rules. But moderators can always decide on a case-by-case basis, so it depends on who you ask.

### Is this a modded client?
No.

### Does this use AI?
No.

### So you programmed all the logic by hand?
By hoof!

### How do they know when to start running?
When the hat appears small enough on screen.

### What happens when hat disappears?
They continue where they were going, then stop and spin around.

### What do I need for this?
- Linux or Unix-like
- a window manager that supports window tiling, removing title bars and hiding screen statusbar (window list)
- multiple Legends of Equestria accounts
- at least one 1920x1080 (full HD) screen
- Python
- xdotool
- scrot

### Can I do this on Windows?
You can try. You will need to rewrite the program in a new way. Don't expect it to work easily.

### So which window manager should I pick?
I have used this both in i3 window manager and in awesome window manager. But you don't need something so "hardcore". Any desktop environment that's configurable enough to allow completely removing window borders is probably fine.

### How do I use this?
1. open Legends of Equestria 4 times, log in with different accounts and hide the UI with `F1`
2. position the 4 windows so that each takes up exactly a quarter of your left-most screen, without window decorations and without the operating system's bar
3. find the window ID of each window (you can do this with the command `xdotool getactivewindow`)
4. put all 4 window IDs into the file `~/.cache/marked_loe_windows` in correct order: first top left, then top right, then bottom left, then bottom right. Example file content:
```
23068692
18874388
20971540
16777236
```
I suggest binding the command `xdotool getactivewindow >>~/.cache/marked_loe_windows` to a keyboard combination so you can trigger it while focusing the LoE window.

5. ensure Linux temporary files are stored in RAM, otherwise this will run quite slowly
6. run the program `loe-follow`
7. put on a hat of color `ff00ff`!

### Can't I do this without having 1920x1080 screens?
You could probably use Xephyr or a similar sandbox to get a virtual screen of the resolution you want. An additional benifit would be that the sandbox would have its own mouse cursor, so my program wouldn't take control of your *real* mouse any more. I haven't gotten far with this though. All I can say is, if you want to run games in Xephyr, you'll want to use virtualgl as described [here](www.reddit.com/r/archlinux/comments/1cxhqj/multiseat_gaming_guide/). And expect mouse camera controls to be broken in Xephyr.
